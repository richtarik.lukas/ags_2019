// Agent aDruid in project jasonTeam.mas2j
{ include("communication.asl") }

/* Initial beliefs and rules */
	
best_spell(A, B, C, D)
	:- spell(A, B, C, D) & not (spell(A2, B2, C2, D2) & A+B+C+D < A2+B2+C2+D2).

have_material(A, B, C, D)
	:- depot(stone, NA) & depot(water, NB) & depot(wood, NC) & depot(gold, ND)
	&  A <= NA & B <= NB & C <= NC & D <= ND.

oponent_is_ahead
	:- money(us, Our_score) & money(them, Enemy_score)
	&  Enemy_score - Our_score >= 10.

gather_resources :- true.
gather_resources 
	:- world(gold, _, _) & world(water, _, _) & world(wood, _, _) 
	&  world(stone, _, _).	

//Testovacie:
//+step(1) <- .broadcast(tell, required_resource(stone)); do(skip).
//+step(N) <- do(skip).

+step(X)
	:  not oponent_is_ahead & depot(pergamen,Y) & Y > 4 
	<- !required_resources;
	   do(read, 5).

+step(X) 
	:  not oponent_is_ahead & best_spell(A, B, C, D) & have_material(A, B, C, D)
	<- !required_resources;
	   do(create, A, B, C, D).

+step(X)
	:  oponent_is_ahead & spell(A, B, C, D) & have_material(A, B, C, D)
	<- !required_resources;
	   do(create, A, B, C, D).

+step(_)
	<- !required_resources;
	   do(skip).

+!required_resources
	:  true
	<- !required(stone);
	   !required(water);
	   !required(wood);
	   !required(gold);
	   !required(pergamen).

+!required(X)
	:  gather_resources & depot(X, Store) & Store < 5
	<- !send_to_friends(tell, required_resource(X)).

+!required(X)
	:  true
	<- !send_to_friends(untell, required_resource(X)).

