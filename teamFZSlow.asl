{ include("communication.asl") }

+step(0) <- 
	+haveSpectacles(3);
	.println("START");
	+wantedThing(nic, nic, nic);
	.setof(Z, friend(Z), Friends);
    .send(Friends, tell, reserved(nic, nic, nic));
	+bResearchUp;
	!decide.
	
+step(N): bEnded <-
	-bEnded;
	.println("New round");
	!decide.

//som v depote. vysypem vsetko
+!decide: moves_left(N) & N>0 & bag([G]) & not(G == null) & haveSpectacles(S) & depot(X,Y) & pos(X, Y) <-	
	.println("Look 1");
	!lookAround(S);
	.println("Som v depote ", G);
	 drop(all);
	 +goToLastResearchedPlace;
	!decide.
	
//mam vodu, idem do depotu	
+!decide: moves_left(N) & N>0 & bag([water]) & haveSpectacles(S) & depot(X,Y) <-
	.println("Look 2");
	!lookAround(S);
	.println("Mam vodu, idem do depotu");
	+goToLastResearchedPlace;
	!goCloser(X, Y);
	!decide.
	
//som plny, idem do depotu	
+!decide: moves_left(N) & N>0 & bag_full & haveSpectacles(S) & depot(X,Y) <-
	.println("Look 3");
	!lookAround(S);
	.println("Som plny, idem do depotu");
	+goToLastResearchedPlace;
	!goCloser(X, Y);
	!decide.
	
//nasiel som v blizkosti okuliare - hladam ich ak ich este nemam	
+!decide: moves_left(N) & N>0 & pos(X1,Y1) & haveSpectacles(S) & S == 3 & nearest(spectacles, X2, Y2) & distance(X1, Y1, X2, Y2, D) & D<3 <-
	.println("Look 4");
	!lookAround(S); // TOTO je OK
	.println("Bring spectacles", X2, " ", Y2);
	!bring(spectacles);
	+goToLastResearchedPlace;
	!decide.
	
//suroviny idem zbierat	 - ak nenajdeme tu co chce druid, ideme hladat dalsiu

+!decide: moves_left(N) & N>0 & not bag_full & haveSpectacles(S) & required_resource(Q) & Q==water & world(water,_,_) <-
	.println("Look 10");
	!lookAround(S);
	+goToLastResearchedPlace;
	.println("Bring ", Q, " ", X, " ", Y);
	!bring(Q);
	!decide.

+!decide: moves_left(N) & N>0 & required_resource(Q) & haveSpectacles(S) & Q==gold & world(Q,X,Y) & not reserved(Q,X,Y) & not world(gloves,X,Y) & not world(shoes,X,Y) <-
	.println("Look 5");
	!lookAround(S);
	+goToLastResearchedPlace;
	.println("Bring ", Q, " ", X, " ", Y);
	!bring(Q);
	!decide.

+!decide: moves_left(N) & N>0 & required_resource(Q) & haveSpectacles(S) & Q==pergamen & world(Q,X,Y) & not reserved(Q,X,Y) & not world(gloves,X,Y) & not world(shoes,X,Y) <-
	.println("Look 8");
	!lookAround(S);
	+goToLastResearchedPlace;
	.println("Bring ", Q, " ", X, " ", Y);
	!bring(Q);
	!decide.
	
+!decide: moves_left(N) & N>0 & required_resource(Q) & haveSpectacles(S) & Q==wood & world(Q,X,Y) & not reserved(Q,X,Y) & not world(gloves,X,Y) & not world(shoes,X,Y) <-
	.println("Look 7");
	!lookAround(S);
	+goToLastResearchedPlace;
	.println("Bring ", Q, " ", X, " ", Y);
	!bring(Q);
	!decide.

+!decide: moves_left(N) & N>0 & required_resource(Q) & haveSpectacles(S) & Q==stone & world(Q,X,Y) & not reserved(Q,X,Y) & not world(gloves,X,Y) & not world(shoes,X,Y) <-
	.println("Look 6");
	!lookAround(S);
	+goToLastResearchedPlace;
	.println("Bring ", Q, " ", X, " ", Y);
	!bring(Q);
	!decide.
	
//----------------------------------------------------------------------------------
//dostan sa na naposledy prehladavanu poziciu

+!decide: moves_left(N) & N>0 & goToLastResearchedPlace & haveSpectacles(S) & pos(X1,Y1) & lastResearchedPos(X, Y) & X1==X & Y1==Y <-
	.println("Look 11");
	!lookAround(S);
	.println("Tuna som skoncil vyhladavanie, idem pokracovat");
	-goToLastResearchedPlace;
	!decide.

+!decide: moves_left(N) & N>0 & goToLastResearchedPlace & haveSpectacles(S) & lastResearchedPos(X, Y) <-
	.println("Look 12");
	!lookAround(S);
	.println("Vraciam sa k poslednemu hladanemu miestu");
	!goCloser(X, Y);
	!decide.

//prehladavanie	------------------------------------------------------------------------
+!decide: moves_left(N) & N>0 & pos(X, Y) & haveSpectacles(S) & (bResearchRight | bTurnLeft1 | bTurnLeft2 | bTurnLeft3 | bResearchLeft | bTurnRight1 | bTurnRight2 | bTurnRight3 | bResearchUp) <-
	!lookAround(S);
	.println("Rozhodujem sa");
	-lastResearchedPos(_,_);
	+lastResearchedPos(X, Y);
	!doResearch;
	!decide.
	
+!decide: haveSpectacles(S) <- 
	+bEnded;
	.println("Look 14");
	!lookAround(S);
	.println("Dosli tahy").
	

//hladanie surovin	
+!bring(Surovina): pos(X,Y) & nearest(Surovina, X1, Y1) & not world(gloves,X1,Y1) & not world(shoes,X1,Y1) & X==X1 & Y==Y1 & (Surovina==gold | Surovina==wood | Surovina==pergamen | Surovina==water) & wantedThing(V,B,N)<-
	.setof(Z, friend(Z), Friends);
    .send(Friends, untell, reserved(V,B,N));
	.abolish(wantedThing(V,B,N));
	+wantedThing(nic, nic, nic);
    .send(Friends, tell, reserved(nic,nic,nic));
	.println("Som na spravnej pozicii, idem zobrat ", Surovina);
	do(pick).

+!bring(Surovina): pos(X,Y) & nearest(Surovina, X1, Y1) & not world(gloves,X1,Y1) & not world(shoes,X1,Y1) & X==X1 & Y==Y1 & Surovina==spectacles & wantedThing(V,B,N)<-	
	.setof(Z, friend(Z), Friends);
    .send(Friends, untell, reserved(V,B,N));
	.abolish(wantedThing(V,B,N));
	+wantedThing(nic, nic, nic);
    .send(Friends, tell, reserved(nic,nic,nic));
	.println("Som na spravnej pozicii, idem zobrat ", Surovina);
	.abolish(haveSpectacles(_));
	+haveSpectacles(6);
	do(pick);
	.
	
+!bring(Surovina): pos(X,Y) & (Surovina==stone) & stone(C,D) & inScope(X-C,Y-D,DIR) & transformToGo(DIR, Side) & wantedThing(V,B,N)<-
	.setof(Z, friend(Z), Friends);
    .send(Friends, untell, reserved(V,B,N));
	.abolish(wantedThing(V,B,N));
	+wantedThing(nic, nic, nic);
    .send(Friends, tell, reserved(nic,nic,nic));
	.println("Som na spravnej pozicii, idem zobrat ", Surovina, " v smere ", DIR, " potom pojdem smerom ", Side);
	do(dig,DIR);.
	
+!bring(Surovina): pos(X,Y) & nearest(Surovina, X1, Y1) & not world(gloves,X1,Y1) & not world(shoes,X1,Y1) & wantedThing(V,B,N)<-
	.setof(Z, friend(Z), Friends);
    .send(Friends, untell, reserved(V,B,N));
	.abolish(wantedThing(V,B,N));
	+wantedThing(Surovina, X1, Y1);
    .send(Friends, tell, reserved(Surovina, X1, Y1));
	.println("Ideme hladat ", Surovina, ", ideme na poziciu: ", X1, " ", Y1);
	!goCloser(X1, Y1).
	
+!bring(Surovina):  wantedThing(V,B,N) <-
	.setof(Z, friend(Z), Friends);
    .send(Friends, untell, reserved(V,B,N));
	.abolish(wantedThing(V,B,N));
	+wantedThing(nic, nic, nic);
	.send(Friends, tell, reserved(nic,nic,nic));
	.println("Nenasiel som hladanu surovinu ", Surovina).
	
distance(X1, Y1, X2, Y2, D) :- D = math.sqrt((X1-X2)**2+(Y1-Y2)**2).
nearest(M, X, Y) :- pos(Px, Py) & world(M, X, Y) & not reserved(M, X, Y) & distance(Px, Py, X, Y, D1)
             &  not (world(M, X2, Y2) & distance(Px, Py, X2, Y2, D2)
             &  D1 > D2 & not world(gloves,X,Y) & not world(spectacles,X,Y)).	


	
//Do research ---------------------------------------------------------------------

+!doResearch: bResearchUp & pos(X,Y) & grid_size(W, H) & (stone(X,Y-1) | not canGoToWater(X,Y-1) | Y==0 | Y==1) <- 
	.println("Zacinam prehladavanie");
	-bResearchUp;
	+bResearchRight.
	
+!doResearch: bResearchUp <-
	.println("Idem hore hladat");
	do(up).

+!doResearch: bResearchRight & pos(X,Y) & grid_size(W, H) & (stone(X+1,Y) | not canGoToWater(X+1,Y) | X==W-2 | X==W-1)<- 
	-bResearchRight;
	.println("Nasli sme prekazku vpravo");
	-bResearchRight;
	+bTurnLeft1.
	
+!doResearch: bResearchRight <- 
	.println("Putujem doprava");
	do(right).

+!doResearch: bResearchLeft & grid_size(W, H) & pos(X,Y) & (stone(X-1,Y) | not canGoToWater(X-1,Y) | X==1 | X==0)<- 
	.println("Nasli sme prekazku vlavo");
	-bResearchLeft;
	+bTurnRight1.
	
+!doResearch: bResearchLeft <- 
	.println("Putujem dolava");
	do(left).

+!doResearch: (bTurnLeft1 | bTurnLeft2 | bTurnLeft3 | bTurnRight1 | bTurnRight2 | bTurnRight3)  & grid_size(W, Z) & pos(X,Y) & (Y == Z-1 | Y == Z-2) <-
	-bTurnLeft1;
	-bTurnLeft2;
	-bTurnLeft3;
	-bTurnRight1;
	-bTurnRight2;
	-bTurnRight3;
	.println("Koncim Vyhladavanie, zacinam odznova");
	+bResearchUp.
	
+!doResearch: bTurnLeft1 & pos(X,Y) & (stone(X,Y+1) | not canGoToWater(X,Y+1)) & (stone(X-1,Y) | not canGoToWater(X-1,Y)) <- 
	-bTurnLeft1;
	+bResearchUp;
	do(up).	
	
+!doResearch: bTurnLeft1 & pos(X,Y) & (stone(X,Y+1) | not canGoToWater(X,Y+1)) <- 
	.println("Otacanie dolava: prekazka");
	do(left).
	

+!doResearch: bTurnRight1 & pos(X,Y) & pos(X,Y) & (stone(X,Y+1) | not canGoToWater(X,Y+1)) & (stone(X+1,Y) | not canGoToWater(X+1,Y)) <- 
	-bTurnRight1;
	+bResearchUp;
	do(up).
	
+!doResearch: bTurnRight1 & pos(X,Y) & pos(X,Y) & (stone(X,Y+1) | not canGoToWater(X,Y+1)) <- 
	.println("Otacanie doprava: prekazka");
	do(right).

	
+!doResearch: bTurnLeft1 <- 
	.println("Otacame dolava");
	do(down);
	-bTurnLeft1;
	+bTurnLeft2.
	
+!doResearch: bTurnRight1 <- 
	.println("Otacame doprava");
	do(down);
	-bTurnRight1;
	+bTurnRight2.


+!doResearch: bTurnLeft2 & pos(X,Y) & (stone(X,Y+1) | not canGoToWater(X,Y+1)) & (stone(X-1,Y) | not canGoToWater(X-1,Y)) <- 
	-bTurnLeft2;
	+bResearchUp;
	do(up).	
	
+!doResearch: bTurnLeft2 & pos(X,Y) & (stone(X,Y+1) | not canGoToWater(X,Y+1)) <- 
	.println("Otacanie dolava: prekazka");
	do(left).


+!doResearch: bTurnRight2 & pos(X,Y) & (stone(X,Y+1) | not canGoToWater(X,Y+1)) & (stone(X+1,Y) | not canGoToWater(X+1,Y)) <- 
	-bTurnRight2;
	+bResearchUp;
	do(up).		
	
+!doResearch: bTurnRight2 & pos(X,Y) & (stone(X,Y+1) | not canGoToWater(X,Y+1)) <- 
	.println("Otacanie doprava: prekazka");
	do(right).
	
+!doResearch: bTurnLeft2 <- 
	.println("Otacame dolava");
	do(down);
	-bTurnLeft2;
	+bTurnLeft3.
	
+!doResearch: bTurnRight2 <- 
	.println("Otacame doprava");
	do(down);
	-bTurnRight2;
	+bTurnRight3.

+!doResearch:  bTurnLeft3 & pos(X,Y) & (stone(X,Y+1) | not canGoToWater(X,Y+1)) & (stone(X-1,Y) | not canGoToWater(X-1,Y)) <- 
	-bTurnLeft3;
	+bResearchUp;
	do(up).		
	
+!doResearch: bTurnLeft3 & pos(X,Y) & (stone(X,Y+1) | not canGoToWater(X,Y+1)) <- 
	.println("Otacanie dolava 3: prekazka");
	do(left).
	

+!doResearch:  bTurnRight3 & pos(X,Y) & (stone(X,Y+1) | not canGoToWater(X,Y+1)) & (stone(X+1,Y) | not canGoToWater(X+1,Y)) <- 
	-bTurnRight3;
	+bResearchUp;
	do(up).		
	
+!doResearch: bTurnRight3 & pos(X,Y) & (stone(X,Y+1) | not canGoToWater(X,Y+1)) <- 
	.println("Otacanie doprava 3: prekazka");
	do(right).
	
+!doResearch: bTurnLeft3 <- 
	.println("Otacanie dolava 3");
	do(down);
	-bTurnLeft3;
	+bResearchLeft.
	
+!doResearch: bTurnRight3 <- 
	.println("Otacanie doprava 3");
	do(down);
	-bTurnRight3;
	+bResearchRight.
	
+!doResearch: True <-
	.println("Neviem co mam robit").

