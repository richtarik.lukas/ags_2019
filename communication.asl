//help function
inScope(-1,0,e).
inScope(1,0,w).
inScope(0,1,n).
inScope(0,-1,s).

transformToGo(w,left).
transformToGo(e,right).
transformToGo(n,up).
transformToGo(s,down).

/////////////////////////////////////////////////////////////

@sendtofr[atomic] +!send_to_friends(Ilf, Message): True
    <- .setof(Z, friend(Z), Friends);
       .send(Friends, Ilf, Message).

@worldPlusSelf[atomic] +world(X, Y, Z)[source(self)]
    :  true
    <- !send_to_friends(tell, world(X, Y, Z)).

@worldPlusName[atomic] +world(X, Y, Z)[source(Name)]
    :  not friend(Name)
    <- .abolish(world(X, Y, Z)[source(Name)]).	

@worldMinusSelf[atomic] -world(X, Y, Z)[source(self)]
    :  true
    <- !send_to_friends(untell, world(X, Y, Z));
	   .abolish(world(X, Y, Z)).

@worldMinusName[atomic] -world(X, Y, Z)[source(Name)]
    :  friend(Name)
    <- .abolish(world(X, Y, Z)).

	
/////////////////////////////////// LookAround
+!lookAround(C): pos(A, B) <-
	for( .range(I, -C, C) )
	{
		for( .range(J, -C, C) )
		{
			!checkPos(A+I,B+J);
		}
	}
	.


+!checkPos(X,Y) : X>=0 & Y >=0 & grid_size(W,H) & X<W & Y<H <-
	!checkPos_Wood(X,Y);
	!checkPos_Pergamen(X,Y);
	!checkPos_Gold(X,Y);
	
	!checkPos_Water(X,Y);
	!checkPos_Stone(X,Y);
	
	!checkPos_Gloves(X,Y);
	!checkPos_Shoes(X,Y);
	!checkPos_Spectacles(X,Y);
	
	!checkPos_Empty(X,Y);
	.

+!checkPos(X,Y): true.
	
// Wood:
+!checkPos_Wood(X, Y): world(wood, X, Y) & not wood(X,Y) <- -world(wood, X, Y);.
+!checkPos_Wood(X, Y): wood(X, Y) <- +world(wood, X, Y);.
+!checkPos_Wood(X, Y): not wood(X, Y).

// Pergamen:
+!checkPos_Pergamen(X, Y): world(pergamen, X, Y) & not pergamen(X,Y) <- -world(pergamen, X, Y);.
+!checkPos_Pergamen(X, Y): pergamen(X, Y) <- +world(pergamen, X, Y);.
+!checkPos_Pergamen(X, Y): not pergamen(X, Y).

// Gold:	
+!checkPos_Gold(X, Y): world(gold, X, Y) & not gold(X, Y) <- -world(gold, X, Y);.
+!checkPos_Gold(X, Y): gold(X, Y) <- +world(gold, X, Y);.
+!checkPos_Gold(X, Y): not gold(X, Y).

// Water:
+!checkPos_Water(X, Y): world(water, X, Y) & not water(X, Y) <- -world(water, X, Y);. //Asi sa voda nestrati ale ...
+!checkPos_Water(X, Y): water(X,Y) <- +world(water, X, Y);.
+!checkPos_Water(X, Y): not water(X, Y).

// Stone:
+!checkPos_Stone(X, Y): world(stone, X, Y) & not stone(X, Y) <- -world(stone, X, Y);.
+!checkPos_Stone(X, Y): stone(X,Y) <- +world(stone, X, Y);.
+!checkPos_Stone(X, Y): not stone(X, Y).

// Shoes:
+!checkPos_Shoes(X, Y): world(shoes, X, Y) & not shoes(X,Y) <- -world(shoes, X, Y);.
+!checkPos_Shoes(X, Y): shoes(X, Y) <- +world(shoes, X, Y);.
+!checkPos_Shoes(X, Y): not shoes(X, Y).

// Spectacles:
+!checkPos_Spectacles(X, Y): world(spectacles, X, Y) & not spectacles(X,Y) <- -world(spectacles, X, Y);.
+!checkPos_Spectacles(X, Y): spectacles(X,Y) <- +world(spectacles, X, Y);.
+!checkPos_Spectacles(X, Y): not spectacles(X,Y).

// Gloves:
+!checkPos_Gloves(X, Y): world(gloves, X, Y) & not gloves(X,Y) <- -world(gloves, X, Y);.
+!checkPos_Gloves(X, Y): gloves(X,Y) <- +world(gloves, X, Y);.
+!checkPos_Gloves(X, Y): not gloves(X,Y).

// Empty:
+!checkPos_Empty(X, Y): world(_, X, Y).
+!checkPos_Empty(X, Y): not world(_, X, Y) <- +world(empty, X, Y).

//chodza ------------------------------------
// +chosenDirections(X,Y,Dir)
//musim skontrolovat ci moj ciel nieje nahodou voda, aby som ju nepovazoval za prekazku
//chcem ist do prava
+!goCloser(X1, Y1): pos(X,Y) & X<X1 & (canGoToWater(X+1,Y) & not stone(X+1,Y)) & not chosenDirections(X,Y,right) & not yAproach <-
	!prepareGoing(X1,Y1);
	+chosenDirections(X, Y, right);
	.println("Vyrovnavam X. Idem doprava, mozem ist doprava");
	do(right).
	
+!goCloser(X1, Y1): pos(X,Y) & X<X1 & (canGoToWater(X,Y-1) & not stone(X,Y-1)) & not chosenDirections(X,Y,up) & not yAproach <-
	!prepareGoing(X1,Y1);
	+chosenDirections(X, Y, up);
	.println("Vyrovnavam X. Idem doprava, musim ist hore");
	do(up).
	
+!goCloser(X1, Y1): pos(X,Y) & X<X1 & (canGoToWater(X,Y+1) & not stone(X,Y+1)) & not chosenDirections(X,Y,down) & not yAproach <-
	!prepareGoing(X1,Y1);
	+chosenDirections(X, Y, down);
	.println("Vyrovnavam X. Idem doprava, musim ist dole");
	do(down).
	
+!goCloser(X1, Y1): pos(X,Y) & X<X1 & (canGoToWater(X-1,Y) & not stone(X-1,Y)) & not chosenDirections(X,Y,left) & not yAproach <-
	!prepareGoing(X1,Y1);
	+chosenDirections(X, Y, left);
	.println("Vyrovnavam X. Idem doprava, musim ist dolava");
	do(left).
	
+!goCloser(X1, Y1): pos(X,Y) & X<X1 & not yAproach <-
	+yAproach;
	.abolish(chosenDirections(_,_,_));
	.println("Vyrovnavam X. Zacyklil som sa ked som siel vpravo").
	
//chcem ist do lava
//---------------------------------------
	
+!goCloser(X1, Y1): pos(X,Y) & X>X1 & (canGoToWater(X-1,Y) & not stone(X-1,Y)) & not chosenDirections(X,Y,left) & not yAproach <-
	!prepareGoing(X1,Y1);
	+chosenDirections(X, Y, left);
	.println("Vyrovnavam X. Idem dolava, mozem ist dolava");
	do(left).
	
+!goCloser(X1, Y1): pos(X,Y) & X>X1 & (canGoToWater(X,Y-1) & not stone(X,Y-1)) & not chosenDirections(X,Y,up) & not yAproach <-
	!prepareGoing(X1,Y1);
	+chosenDirections(X, Y, up);
	.println("Vyrovnavam X. Idem dolava, musim ist hore");
	do(up).
	
+!goCloser(X1, Y1): pos(X,Y) & X>X1 & (canGoToWater(X,Y+1) & not stone(X,Y+1)) & not chosenDirections(X,Y,down) & not yAproach <-
	!prepareGoing(X1,Y1);
	+chosenDirections(X, Y, down);
	.println("Vyrovnavam X. Idem dolava, musim ist dole");
	do(down).
	
+!goCloser(X1, Y1): pos(X,Y) & X>X1 & (canGoToWater(X+1,Y) & not stone(X+1,Y)) & not chosenDirections(X,Y,right) & not yAproach <-
	!prepareGoing(X1,Y1);
	+chosenDirections(X, Y, right);
	.println("Vyrovnavam X. Idem dolava, musim ist doprava");
	do(right).
	
+!goCloser(X1, Y1): pos(X,Y) & X>X1 & not yAproach <-
	+yAproach;
	.abolish(chosenDirections(_,_,_));
	.println("Vyrovnavam X. Zacyklil som sa ked som chcel ist vlavo").
	
	
//X sedi	
+!goCloser(X1, Y1): pos(X,Y) & X==X1 & not yAproach <-
	+yAproach;
	.println("X sedi, idem vyrovnat Y").

//chcem ist hore
//------------------------------------------	
	
+!goCloser(X1, Y1): pos(X,Y) & Y>Y1 & (canGoToWater(X,Y-1) & not stone(X,Y-1)) & not chosenDirections(X,Y,up) & yAproach<-
	!prepareGoing(X1,Y1);
	+chosenDirections(X, Y, up);
	.println("Vyrovnavam Y. Idem hore, mozem ist hore");
	do(up).
	
+!goCloser(X1, Y1): pos(X,Y) & Y>Y1 & (canGoToWater(X-1,Y) & not stone(X-1,Y)) & not chosenDirections(X,Y,left) & yAproach<-
	!prepareGoing(X1,Y1);
	+chosenDirections(X, Y, left);
	.println("Vyrovnavam Y. Idem hore, musim ist dolava");
	do(left).
	
+!goCloser(X1, Y1): pos(X,Y) & Y>Y1 & (canGoToWater(X+1,Y) & not stone(X+1,Y)) & not chosenDirections(X,Y,right) & yAproach<-
	!prepareGoing(X1,Y1);
	+chosenDirections(X, Y, right);
	.println("Vyrovnavam Y. Idem hore, musim ist doprava");
	do(right).
	
+!goCloser(X1, Y1): pos(X,Y) & Y>Y1 & (canGoToWater(X,Y+1) & not stone(X,Y+1)) & not chosenDirections(X,Y,down) & yAproach<-
	!prepareGoing(X1,Y1);
	+chosenDirections(X, Y, down);
	.println("Vyrovnavam Y. Idem hore, musim ist dole");
	do(down).
	
+!goCloser(X1, Y1): pos(X,Y) & Y>Y1 & yAproach<-
	-yAproach;
	.abolish(chosenDirections(_,_,_));
	.println("Vyrovnavam Y. Zacyklil som sa ked som chcel ist hore").
	
//chcem ist dole
//------------------------------------------------	
	+!goCloser(X1, Y1): pos(X,Y) & Y<Y1 & (canGoToWater(X,Y+1) & not stone(X,Y+1)) & not chosenDirections(X,Y,down) & yAproach<-
	!prepareGoing(X1,Y1);
	+chosenDirections(X, Y, down);
	.println("Vyrovnavam Y. Idem dole, mozem ist dole");
	do(down).
	
+!goCloser(X1, Y1): pos(X,Y) & Y<Y1 & (canGoToWater(X-1,Y) & not stone(X-1,Y)) & not chosenDirections(X,Y,left) & yAproach<-
	!prepareGoing(X1,Y1);
	+chosenDirections(X, Y, left);
	.println("Vyrovnavam Y. Idem dole, musim ist dolava");
	do(left).		
	
+!goCloser(X1, Y1): pos(X,Y) & Y<Y1 & (canGoToWater(X+1,Y) & not stone(X+1,Y)) & not chosenDirections(X,Y,right) & yAproach<-
	!prepareGoing(X1,Y1);
	+chosenDirections(X, Y, right);
	.println("Vyrovnavam Y. Idem dole, musim ist doprava");
	do(right).
	
+!goCloser(X1, Y1): pos(X,Y) & Y<Y1 & (canGoToWater(X,Y-1) & not stone(X,Y-1)) & not chosenDirections(X,Y,up) & yAproach<-
	!prepareGoing(X1,Y1);
	+chosenDirections(X, Y, up);
	.println("Vyrovnavam Y. Idem dole, musim ist hore");
	do(up).
	
+!goCloser(X1, Y1): pos(X,Y) & Y<Y1 & yAproach<-
	-yAproach;
	.abolish(chosenDirections(_,_,_));
	.println("Vyrovnavam Y. Zacyklil som sa ked som chcel ist dole").
	
//y je zladene
+!goCloser(X1, Y1): pos(X,Y) & Y==Y1 & yAproach<-
	-yAproach;
	.println("Y sedi, idem vyrovnat X").
	
/////////////////////////
	
//idem na rovnake miesto - nerobim nic
+!prepareGoing(X1,Y1): gone(X,Y) & X1==X & Y1==Y	<-
	+gone(X,Y);
	.println("Ideme k tomu istemu cielu co minule").

//idem na ine miesto
+!prepareGoing(X1,Y1): True <-
	-gone(_,_);
	+gone(X1,Y1);
	.abolish(chosenDirections(_,_,_));
	.println("Ideme k novemu cielu").

canGoToWater(X, Y):- ( (not world(water, X, Y))     & world(_, X, Y) )     |
					 ( (not world(water, X, Y+1))   & world(_, X, Y+1) )   |
					 ( (not world(water, X, Y-1))   & world(_, X, Y-1) )   |
					 ( (not world(water, X+1, Y))   & world(_, X+1, Y) )   |
					 ( (not world(water, X+1, Y+1)) & world(_, X+1, Y+1) ) |
					 ( (not world(water, X+1, Y-1)) & world(_, X+1, Y-1) ) |
					 ( (not world(water, X-1, Y))   & world(_, X-1, Y) )   |
					 ( (not world(water, X-1, Y+1)) & world(_, X-1, Y+1) ) |
					 ( (not world(water, X-1, Y-1)) & world(_, X-1, Y-1) ).


