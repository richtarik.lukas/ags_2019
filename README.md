PROBLEM!!! - Ak na policku kde chcem zobrat surovinu je aj specialny predmet ktory nepatri danemu agentovi, agent sa ho snazi zobrat a zacykli sa


# AGS_2019 **Druidský les**

| Agent              | login    | Priorita                            | item                                      | Štartovacie prehľadávanie | Popis |
| ------------------ | -------- | ----------------------------------- | ----------------------------------------- | ------------------------- | ----- |
| Slow - Bystrozraký | xopich00 | Voda, Zlato/Pergament, Drevo/Kameň  | Ide pre Item len do vzdialenosti "todo s" | Ide smerom k sídlu (Depotu) a prehľadá jeho okolie + hned nosí | |
| Middle - Široký    | xmolit00 | Drevo/Kameň, Zlato, Pergament, Voda | Ide pre Item len do vzdialenosti "todo m" | Ide smerom k Okoliu sídla a prehľadáva + hned nosí | |
| Fast - Rýchly      | xricht25 | Zlato, Drevo/Kameň/Pergament, Voda  | Ide pre Item len do vzdialenosti "todo f" | Ide ďalej, prehľadáva najväčšie okolie pokiaľ sa nenájde voda a les | |
| Druid              | xondre09 | Vyrábať, Čítať                      |          --                               | --                                                  | |

* f > m > s

**Bugs:**

**Todo:**


**Info:**

* Voda - Todo Popis ako funguje voda

**Požadavky druida** - druid přidává/odebíra jednotlivým agentům znalost o potřebných surovinách pro „kouzlení“.
```prolog
required_resource(gold)
required_resource(water)
...
```
**Mapa** - vedomosti kt. nájdeme na mape:
```prolog
world(water, X, Y)
world(gold, X, Y)
world(wood, X, Y)
world(stone, X, Y)
world(pergamen, X, Y)

world(shoes, X, Y)
world(gloves, X, Y)
world(spectacles, X, Y)

world(empty, X, Y)
```
Odoberanie:

```prolog 
-world(A, X, Y)[source(Name)] : true <- .abolish(world(A, X, Y)). //Todo check Name - friends
```
**broadcastneme všetkým vo formáte:**

pridanie príklad: `` .broadcast(tell, world(gold,X,Y)); ``

odobranie príklad: `` .broadcast(untell, world(gold,X,Y)); ``

(Slow - Bystrozraký neodosiela Informáciu o okuliaroch ale rovno si ich vezme atď...)
