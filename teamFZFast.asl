{ include("communication.asl") }

// Priority
//prioritny tah (pri tazbe kamena)
// Ak som v depote, vysypem
// Ak mam v ruksaku vodu, idem do depotu
// Ak mam plny ruksak idem prioritne do depotu
// Ak mam v urcitej vzdialenosti topanky - vezmem ich
// Ak druid chce nejake suroviny - idem po nich podla priority - Zlato, Drevo, Kamen, Pergament, Voda
// Vratim sa na posledne prehladavane miesto
// Prehladavam

+step(0) <- 
	.println("START");
	+wantedThing(nic, nic, nic);
	.setof(Z, friend(Z), Friends);
    .send(Friends, tell, reserved(nic, nic, nic));
	+bResearchLeftNew;
	!decide.
	
+step(N): bEnded <-
	-bEnded;
	.println("New round");
	!decide.

//prioritny tah
+!decide: moves_left(N) & N>0 & priorityStep(DIR) <-	
	.println("Idem sa pozriet na dalsi potencionalny kamen");
	!lookAround(1);
	do(DIR);
	-priorityStep(DIR);
	!decide.

//som v depote. vysypem vsetko
+!decide: moves_left(N) & N>0 & bag([G,_]) & not(G == null) & depot(X,Y) & pos(X1, Y1) & X1 == X & Y1==Y <-	
	.println("Look 1");
	!lookAround(1);
	.println("Som v depote ", G);
	 drop(all);
	 +goToLastResearchedPlace;
	!decide.
	
//mam vodu, idem do depotu	
+!decide: moves_left(N) & N>0 & bag([water,_]) & depot(X,Y) <-
	.println("Look 2");
	!lookAround(1);
	.println("Mam vodu, idem do depotu");
	+goToLastResearchedPlace;
	!goCloser(X, Y);
	!decide.
	
//som plny, idem do depotu	
+!decide: moves_left(N) & N>0 & bag_full & depot(X,Y) <-
	.println("Look 3");
	!lookAround(1);
	.println("Som plny, idem do depotu");
	+goToLastResearchedPlace;
	!goCloser(X, Y);
	!decide.
	
//nasiel som v blizkosti topanku - hladam ju ak som ju este nenasiel	
+!decide: moves_left(N) & N>0 & pos(X1,Y1) & not haveShoes & nearest(shoes, X2, Y2) & distance(X1, Y1, X2, Y2, D) & D<9 <-
	.println("Look 4");
	!lookAround(1);
	.println("Bring shoes", X2, " ", Y2);
	!bring(shoes);
	+goToLastResearchedPlace;
	!decide.
	
//suroviny idem zbierat	 - ak nenajdeme tu co chce druid, ideme hladat dalsiu	
+!decide: moves_left(N) & N>0 & required_resource(Q) & Q==gold & world(Q,X,Y) & not reserved(Q,X,Y) & not world(gloves,X,Y) & not world(spectacles,X,Y) <-
	.println("Look 5");
	!lookAround(1);
	+goToLastResearchedPlace;
	.println("Bring ", Q, " ", X, " ", Y);
	!bring(Q);
	!decide.

+!decide: moves_left(N) & N>0 & required_resource(Q) & Q==stone & world(Q,X,Y) & not reserved(Q,X,Y) & not world(gloves,X,Y) & not world(spectacles,X,Y) <-
	.println("Look 6");
	!lookAround(1);
	+goToLastResearchedPlace;
	.println("Bring ", Q, " ", X, " ", Y);
	!bring(Q);
	!decide.
	
+!decide: moves_left(N) & N>0 & required_resource(Q) & Q==wood & world(Q,X,Y) & not reserved(Q,X,Y) & not world(gloves,X,Y) & not world(spectacles,X,Y) <-
	.println("Look 7");
	!lookAround(1);
	+goToLastResearchedPlace;
	.println("Bring ", Q, " ", X, " ", Y);
	!bring(Q);
	!decide.
	
+!decide: moves_left(N) & N>0 & required_resource(Q) & Q==pergamen & world(Q,X,Y) & not reserved(Q,X,Y) & not world(gloves,X,Y) & not world(spectacles,X,Y) <-
	.println("Look 8");
	!lookAround(1);
	+goToLastResearchedPlace;
	.println("Bring ", Q, " ", X, " ", Y);
	!bring(Q);
	!decide.
	
//ak mam nieco v taske, napr kamen a chcem vodu, musim najprv odniest do depotu
+!decide: moves_left(N) & N>0 & required_resource(Q) & Q==water & depot(X,Y) & bag([G,_]) & not(G == null) & world(water,_,_) <-
	.println("Look 9");
	!lookAround(1);
	.println("Chcem vodu, musim odniest vsetko co mam v ruksaku");
	+goToLastResearchedPlace;
	!goCloser(X, Y);
	!decide.
	
+!decide: moves_left(N) & N>0 & required_resource(Q) & Q==water & world(water,_,_) <-
	.println("Look 10");
	!lookAround(1);
	+goToLastResearchedPlace;
	.println("Bring ", Q, " ", X, " ", Y);
	!bring(Q);
	!decide.

	
//----------------------------------------------------------------------------------
//dostan sa na naposledy prehladavanu poziciu

+!decide: moves_left(N) & N>0 & goToLastResearchedPlace & pos(X1,Y1) & lastResearchedPos(X, Y) & X1==X & Y1==Y <-
	.println("Look 11");
	!lookAround(1);
	.println("Tuna som skoncil vyhladavanie, idem pokracovat");
	-goToLastResearchedPlace;
	!decide.

+!decide: moves_left(N) & N>0 & goToLastResearchedPlace & lastResearchedPos(X, Y) <-
	.println("Look 12");
	!lookAround(1);
	.println("Vraciam sa k poslednemu hladanemu miestu");
	!goCloser(X, Y);
	!decide.
	
//prehladavanie	------------------------------------------------------------------------
+!decide: moves_left(N) & N>0 & pos(X, Y) & (bResearchUpNew | bTurnDown1 | bTurnDown2 | bTurnDown3 | bResearchDownNew | bTurnUp1 | bTurnUp2 | bTurnUp3 | bResearchLeftNew) <-
	.println("Look 13");
	!lookAround(1);
	-lastResearchedPos(_,_);
	+lastResearchedPos(X, Y);
	!doResearch;
	!decide.
	
+!decide:True <- 
	+bEnded;
	.println("Look 14");
	!lookAround(1);
	.println("Dosli tahy").
	

//hladanie surovin	
+!bring(Surovina): pos(X,Y) & nearest(Surovina, X1, Y1) & not world(gloves,X1,Y1) & not world(spectacles,X1,Y1) & X==X1 & Y==Y1 & (Surovina==gold | Surovina==wood | Surovina==pergamen | Surovina==water) & wantedThing(V,B,N)<-
	.setof(Z, friend(Z), Friends);
    .send(Friends, untell, reserved(V,B,N));
	.abolish(wantedThing(V,B,N));
	+wantedThing(nic, nic, nic);
    .send(Friends, tell, reserved(nic,nic,nic));
	.println("Som na spravnej pozicii, idem zobrat ", Surovina);
	do(pick).

+!bring(Surovina): pos(X,Y) & nearest(Surovina, X1, Y1) & not world(gloves,X1,Y1) & not world(spectacles,X1,Y1) & X==X1 & Y==Y1 & Surovina==shoes & wantedThing(V,B,N)<-	
	.setof(Z, friend(Z), Friends);
    .send(Friends, untell, reserved(V,B,N));
	.abolish(wantedThing(V,B,N));
	+wantedThing(nic, nic, nic);
    .send(Friends, tell, reserved(nic,nic,nic));
	.println("Som na spravnej pozicii, idem zobrat ", Surovina);
	do(pick);
	+haveShoes.
	
+!bring(Surovina): pos(X,Y) & (Surovina==stone) & stone(C,D) & inScope(X-C,Y-D,DIR) & transformToGo(DIR, Side) & wantedThing(V,B,N)<-
	.setof(Z, friend(Z), Friends);
    .send(Friends, untell, reserved(V,B,N));
	.abolish(wantedThing(V,B,N));
	+wantedThing(nic, nic, nic);
    .send(Friends, tell, reserved(nic,nic,nic));
	.println("Som na spravnej pozicii, idem zobrat ", Surovina, " v smere ", DIR, " potom pojdem smerom ", Side);
	do(dig,DIR);
	+priorityStep(Side).
	
+!bring(Surovina): pos(X,Y) & nearest(Surovina, X1, Y1) & not world(gloves,X1,Y1) & not world(spectacles,X1,Y1) & wantedThing(V,B,N) <-
	.setof(Z, friend(Z), Friends);
    .send(Friends, untell, reserved(V,B,N));
	.abolish(wantedThing(V,B,N));
	+wantedThing(Surovina, X1, Y1);
    .send(Friends, tell, reserved(Surovina, X1, Y1));
	.println("Ideme hladat ", Surovina, ", ideme na poziciu: ", X1, " ", Y1);
	!goCloser(X1, Y1).
	
+!bring(Surovina): wantedThing(V,B,N) <-
	.setof(Z, friend(Z), Friends);
    .send(Friends, untell, reserved(V,B,N));
	.abolish(wantedThing(V,B,N));
	+wantedThing(nic, nic, nic);
	.send(Friends, tell, reserved(nic,nic,nic));
	.println("Nenasiel som hladanu surovinu ", Surovina).
	
distance(X1, Y1, X2, Y2, D) :- D = math.sqrt((X1-X2)**2+(Y1-Y2)**2).
nearest(M, X, Y) :- pos(Px, Py) & world(M, X, Y) & not reserved(M, X, Y) & distance(Px, Py, X, Y, D1)
             &  not (world(M, X2, Y2) & distance(Px, Py, X2, Y2, D2)
             &  D1 > D2 & not world(gloves,X,Y) & not world(spectacles,X,Y)).	

//Do research ---------------------------------------------------------------------

+!doResearch: bResearchLeftNew & pos(X,Y) & grid_size(W, H) & (stone(X-1,Y) | not canGoToWater(X-1,Y) | X==0 | X==1) <- 
	.println("Dorazil som dolava, idem prehladavat hore");
	-bResearchLeftNew;
	+bResearchUpNew.
	
+!doResearch: bResearchLeftNew <-
	.println("Idem dolava prehladavat");
	do(left).

+!doResearch: bResearchUpNew & pos(X,Y) & grid_size(W, H) & (stone(X,Y-1) | not canGoToWater(X,Y-1) | Y==0 | Y==1)<- 
	-bResearchUpNew;
	.println("Nasli sme prekazku hore, otacam sa");
	+bTurnDown1.
	
+!doResearch: bResearchUpNew <- 
	.println("Putujem hore");
	do(up).

+!doResearch: bResearchDownNew & grid_size(W, H) & pos(X,Y) & (stone(X,Y+1) | not canGoToWater(X,Y+1) | Y==H-1 | Y==H-2)<- 
	.println("Nasli sme prekazku dole, otacam sa");
	-bResearchDownNew;
	+bTurnUp1.
	
+!doResearch: bResearchDownNew <- 
	.println("Putujem dole");
	do(down).

+!doResearch: (bTurnUp1 | bTurnUp2 | bTurnUp3 | bTurnDown1 | bTurnDown2 | bTurnDown3)  & grid_size(W, Z) & pos(X,Y) & (X == W-1 | X == W-2) <-
	-bTurnUp1;
	-bTurnUp2;
	-bTurnUp3;
	-bTurnDown1;
	-bTurnDown2;
	-bTurnDown3;
	.println("Koncim Vyhladavanie, zacinam odznova");
	+bResearchLeftNew.

//neda sa ist ani dole	- vlavo nebude osetrene
+!doResearch: bTurnDown1 & pos(X,Y) & (stone(X+1,Y) | not canGoToWater(X+1,Y)) & (stone(X,Y+1) | not canGoToWater(X,Y+1)) <- 
	.println("Otacanie dole 1: prekazka aj dole pri cuvani");
	-bTurnDown1;
	+bResearchLeftNew;
	do(left).
	
+!doResearch: bTurnDown1 & pos(X,Y) & (stone(X+1,Y) | not canGoToWater(X+1,Y)) <- 
	.println("Otacanie dole 1: prekazka");
	do(down).

//neda sa ist ani hore	
+!doResearch: bTurnUp1 & pos(X,Y) & pos(X,Y) & (stone(X+1,Y) | not canGoToWater(X+1,Y)) & (stone(X,Y-1) | not canGoToWater(X,Y-1)) <- 
	.println("Otacanie hore 1: prekazka aj hore pri cuvani");
	-bTurnUp1;
	+bResearchLeftNew;
	do(left).
	
+!doResearch: bTurnUp1 & pos(X,Y) & pos(X,Y) & (stone(X+1,Y) | not canGoToWater(X+1,Y)) <- 
	.println("Otacanie hore 1: prekazka");
	do(up).
	
+!doResearch: bTurnDown1 <- 
	.println("Otacame dole 1");
	do(right);
	-bTurnDown1;
	+bTurnDown2.
	
+!doResearch: bTurnUp1 <- 
	.println("Otacame hore 1");
	do(right);
	-bTurnUp1;
	+bTurnUp2.

//neda sa ist ani dole
+!doResearch: bTurnDown2 & pos(X,Y) & (stone(X+1,Y) | not canGoToWater(X+1,Y)) & (stone(X,Y+1) | not canGoToWater(X,Y+1)) <- 
	.println("Otacanie dole 2: prekazka aj dole pri cuvani");
	-bTurnDown2;
	+bResearchLeftNew;
	do(left).
	
+!doResearch: bTurnDown2 & pos(X,Y) & (stone(X+1,Y) | not canGoToWater(X+1,Y)) <- 
	.println("Otacanie dole 2: prekazka");
	do(down).

//neda sa ist ani hore	
+!doResearch: bTurnUp2 & pos(X,Y) & (stone(X+1,Y) | not canGoToWater(X+1,Y)) & (stone(X,Y-1) | not canGoToWater(X,Y-1)) <- 
	.println("Otacanie hore 2: prekazka aj hore pri cuvani");
	-bTurnUp2;
	+bResearchLeftNew;
	do(left).	
	
+!doResearch: bTurnUp2 & pos(X,Y) & (stone(X+1,Y) | not canGoToWater(X+1,Y)) <- 
	.println("Otacanie hore 2: prekazka");
	do(up).
	
+!doResearch: bTurnDown2 <- 
	.println("Otacame dole 2");
	do(right);
	-bTurnDown2;
	+bTurnDown3.
	
+!doResearch: bTurnUp2 <- 
	.println("Otacame hore 2");
	do(right);
	-bTurnUp2;
	+bTurnUp3.
	
//nemozem ist ani dole
+!doResearch: bTurnDown3 & pos(X,Y) & (stone(X+1,Y) | not canGoToWater(X+1,Y)) & (stone(X,Y+1) | not canGoToWater(X,Y+1)) <- 
	.println("Otacanie dole 3: prekazka aj dole pri cuvani");
	-bTurnDown3;
	+bResearchLeftNew;
	do(left).
	
+!doResearch: bTurnDown3 & pos(X,Y) & (stone(X+1,Y) | not canGoToWater(X+1,Y)) <- 
	.println("Otacanie dole 3: prekazka");
	do(down).

//nemozem ist ani hore
+!doResearch: bTurnUp3 & pos(X,Y) & (stone(X+1,Y) | not canGoToWater(X+1,Y)) <- 
	.println("Otacanie hore 3: prekazka aj hore pri cuvani");
	-bTurnUp3;
	+bResearchLeftNew;
	do(left).
	
+!doResearch: bTurnUp3 & pos(X,Y) & (stone(X+1,Y) | not canGoToWater(X+1,Y)) <- 
	.println("Otacanie hore 3: prekazka");
	do(up).
	
+!doResearch: bTurnDown3 <- 
	.println("Otacanie dole 3");
	do(right);
	-bTurnDown3;
	+bResearchDownNew.
	
+!doResearch: bTurnUp3 <- 
	.println("Otacanie hore 3");
	do(right);
	-bTurnUp3;
	+bResearchUpNew.
	
+!doResearch: True <-
	.println("Neviem co mam robit").
	
	
